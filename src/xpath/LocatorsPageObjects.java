package xpath;

import org.openqa.selenium.By;

public class LocatorsPageObjects {
	
	By tab_RegistrationForm = By.id("professional-development-workshop-registration-form");
	By txt_Prefix = By.xpath("//input[@id='last_3']/preceding::input[@id='prefix_3']");
	By txt_FirstName = By.xpath("//input[@id='last_3']//parent::span//preceding::input[@id='first_3']");
	By txt_LastName  = By.xpath("//*[@id='last_3']//self::input");
	By lnk_Templates = By.linkText("Templates");
	By lnk_MyForms = By.partialLinkText("My");
	By txt_Address = By.cssSelector("input#input_5_addr_line1");
	By txt_Address2 = By.xpath("//input[contains(@id,'input_5_addr_line2')]");
	By txt_AreaCode = By.xpath("//label[@id='label_6']//following::div[2]/span[1]/input"); 
	By txt_PhoneNumber = By.xpath("//label[@id='label_6']//following-sibling::div/div/span[2]/input");
	By txt_City = By.xpath("//label[@id='sublabel_5_city']//ancestor::span/input");
	By txt_State = By.xpath("//table[@class='form-address-table']//child::span/input[@id='input_5_state']");
	By txt_PostalCode = By.xpath("//*[@class='form-address-table']//descendant::input[5]");
	By drp_country = By.id("input_5_country");
	By btn_SubmitForm = By.xpath("//button[contains(text(),'Submit Form')]");
	By btn_Close = By.xpath("//*[@class='modalbox-close']");
	By lnk_ProductOrderForm = By.xpath("//div[@id='conference-online-registration-form']/img");
	By txt_FirstName_0 = By.cssSelector("input#first_2");
	By txt_Next_0 = By.cssSelector("button.form-pagebreak-next ");
	By txt_MiddleName_0 = By.cssSelector("input[id=middle_2]");
	By txt_LastName_0 = By.cssSelector("input.form-textbox[id=last_2]");
	By txt_Email = By.id("input_3");
	By txt_AreaCode_0 = By.name("q5_contactNumber[area]");
	
	

}
