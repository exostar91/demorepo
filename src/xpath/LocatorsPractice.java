package xpath;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LocatorsPractice {
	
	public static void main(String[] args){
		
		LocatorsPageObjects locatorsPageObjects = new LocatorsPageObjects();
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\JarFiles\\ChromeDriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.jotform.com/form-templates/category/registration");	
		
		pause(10);
	
		findElement(driver, locatorsPageObjects.tab_RegistrationForm).click();
		
		pause(10);
	
		WebElement fr = driver.findElement(By.className("form-iframe"));

		driver.switchTo().frame(fr);
		pause(10);
		findElement(driver, locatorsPageObjects.txt_Prefix).sendKeys("Mr.");
		
		findElement(driver, locatorsPageObjects.txt_FirstName).sendKeys("Ratnesh");
		
		findElement(driver, locatorsPageObjects.txt_LastName).sendKeys("Singh");
		
		findElement(driver, locatorsPageObjects.txt_Address).sendKeys("Main Street");
		
		findElement(driver, locatorsPageObjects.txt_Address2).sendKeys("Main Street");
		
		findElement(driver, locatorsPageObjects.txt_City).sendKeys("Ahmedabad");
		
		findElement(driver, locatorsPageObjects.txt_State).sendKeys("Gujrat");
		
		findElement(driver, locatorsPageObjects.txt_PostalCode).sendKeys("350052");
		
		Select select = new Select(findElement(driver, locatorsPageObjects.drp_country));
		select.selectByVisibleText("United States");
		
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		
		pause(1);
		
		findElement(driver, locatorsPageObjects.txt_AreaCode).sendKeys("0754");
		
		findElement(driver, locatorsPageObjects.txt_PhoneNumber).sendKeys("1478545");
				
			
		findElement(driver, locatorsPageObjects.btn_SubmitForm).click();
		
		
		
		pause(5);
				
		System.out.println("Done");
		
		driver.close();
		
	}

	public static void pause(int seconds){
		try {
			Thread.sleep(1000*seconds);	
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static WebElement findElement(WebDriver driver, By by){
		pause(1);
		return driver.findElement(by);
	}
	
}
