package xpath;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class links {
public static void main(String[] args){
		
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\JarFiles\\ChromeDriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.softwebsolutions.com");	
		
		pause(10);
		
		List<WebElement> links = driver.findElements(By.tagName("a"));
		
		System.out.println(links.size());
		
		for(WebElement link : links){
			String a = link.getAttribute("href");
			System.out.println(a);
			driver.navigate().to(a);
			pause(5);
			driver.navigate().back();
			pause(5);
			
		}
}

public static void pause(int seconds){
	try {
		Thread.sleep(1000*seconds);	
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
